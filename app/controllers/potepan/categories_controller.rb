class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxonomy.includes(:root)
    @taxon = Spree::Taxon.find(params[:id])
    @option_types = Spree::OptionType.includes(:option_values).search_by_taxon(@taxon.id)
    @products = filtered_products
  end

  private

  def filtered_products
    scope = Spree::Product.includes_price_image.in_taxon(@taxon)
    params[:option_value].present? ? scope.search_by_option_value(params[:option_value]) : scope
  end
end
