Spree::Product.class_eval do
  # 関連しているpuroductを取得
  def related_products
    Spree::Product.in_taxons(taxons).where.not(id: id).distinct
  end

  scope :includes_price_image, -> { includes(master: [:default_price, :images]) }

  def self.search_by_option_value(option_value)
    joins(variants: :option_values).where(spree_option_values: { id: option_value })
  end
end
