require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario "visit show" do
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
    expect(page).to have_content related_product.name
    expect(page).to have_content related_product.display_price

    click_on related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
