require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon], option_types: [option_type]) }
  let!(:option_type) { create(:option_type) }
  let!(:option_value) { create(:option_value, option_type: option_type) }
  let!(:variant) { create(:variant, product: product, option_values: [option_value]) }

  scenario 'CategoriesShow has taxonomy&taxon&product' do
    visit potepan_category_path(taxon.id)
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price

    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
    expect(page).to have_content product.name
  end

  scenario 'CategoriesShow has option_type&option_value' do
    visit potepan_category_path(taxon.id)
    expect(page).to have_content option_type.presentation
    expect(page).to have_content option_value.name

    click_on option_value.name
    expect(current_path).to eq potepan_category_path(taxon.id)
    expect(page).to have_content product.name
  end
end
