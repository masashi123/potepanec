require 'rails_helper'

RSpec.feature "Samples", type: :feature do
  let(:taxon) { create(:taxon) }
  let!(:new_product) { create(:product, available_on: 1.day.ago, taxons: [taxon]) }

  scenario "visit index" do
    visit potepan_path
    expect(page).to have_content new_product.name
    expect(page).to have_content new_product.display_price

    click_on new_product.name
    expect(current_path).to eq potepan_product_path(new_product.id)
  end
end
