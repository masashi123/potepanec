require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:products) { create(:product, taxons: [taxon]) }
  let(:other_products) { create(:product) }

  describe 'get categories/show' do
    before { get :show, params: { id: taxon.id } }

    it "responds successfully" do
      expect(response).to have_http_status "200"
    end

    it "renders the categories/show" do
      expect(response).to render_template :show
    end

    it "matches the taxonomies" do
      expect(assigns(:taxonomies)).to match_array(taxonomy)
    end

    it "matches the taxon" do
      expect(assigns(:taxon)).to match(taxon)
    end

    it "matches the products" do
      expect(assigns(:products)).to match_array(products)
    end

    it "doesn't show products in taxon" do
      expect(assigns(:products)).not_to match_array(other_products)
    end
  end
end
