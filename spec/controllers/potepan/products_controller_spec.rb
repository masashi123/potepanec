require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before { get :show, params: { id: product.id } }

    it "responds successfully" do
      expect(response).to have_http_status "200"
    end

    it "shows @product" do
      expect(assigns(:product)).to eq product
    end

    it "renders the :show" do
      expect(response).to render_template :show
    end

    it "shows 4 @related_products" do
      expect(assigns(:related_products).length).to eq 4
    end
  end
end
