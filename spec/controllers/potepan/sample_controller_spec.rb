require 'rails_helper'

RSpec.describe Potepan::SampleController, type: :controller do
  describe "#index" do
    let!(:product_1) { create(:product, available_on: 1.day.ago) }
    let!(:product_2) { create(:product, available_on: 2.day.ago) }
    let!(:product_3) { create(:product, available_on: 3.day.ago) }
    let!(:product_4) { create(:product, available_on: 4.day.ago) }
    let!(:product_5) { create(:product, available_on: 5.day.ago) }
    let!(:product_6) { create(:product, available_on: 6.day.ago) }

    before do
      get :index
    end

    it "responds successfully" do
      expect(response).to have_http_status "200"
    end

    it "shows @new_products" do
      expect(assigns(:new_products).first).to eq product_1
    end

    it "renders the :index" do
      expect(response).to render_template :index
    end

    it "shows 5 @new_products" do
      expect(assigns(:new_products).length).to eq 5
    end
  end
end
