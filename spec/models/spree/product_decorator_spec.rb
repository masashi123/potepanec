require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "get ralated_products" do
    let(:taxon_1) { create(:taxon) }
    let(:taxon_2) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon_1]) }
    let!(:related_product_taxon_1) { create(:product, taxons: [taxon_1]) }
    let!(:other_product_taxon_2) { create(:product, taxons: [taxon_2]) }

    let!(:option_type) { create(:option_type) }
    let!(:option_value) { create(:option_value) }
    let!(:variant) { create(:variant, product: product, option_values: [option_value]) }

    it "contains only related_product" do
      expect(product.related_products).to match_array related_product_taxon_1
    end

    it "doesn't contain other_product" do
      expect(product.related_products).not_to match_array other_product_taxon_2
    end

    it "gets product by search_by_option_value" do
      expect(Spree::Product.search_by_option_value(option_value)).to match_array product
    end

    it "doesn't get related_product_taxon_1 by search_by_option_value" do
      expect(Spree::Product.search_by_option_value(option_value)).not_to match_array related_product_taxon_1
    end
  end
end
