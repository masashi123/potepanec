require 'rails_helper'

RSpec.describe Spree::OptionType, type: :model do
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon], option_types: [option_type]) }
  let!(:variant) { create(:variant, product: product, option_values: [option_value]) }
  let!(:option_type) { create(:option_type) }
  let!(:option_value) { create(:option_value, option_type: option_type) }

  it "gets option_type by search_by_taxon" do
    expect(Spree::OptionType.search_by_taxon(taxon.id)).to match_array option_type
  end
end
