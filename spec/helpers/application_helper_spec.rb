require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    include ApplicationHelper

    context "there is page_title" do
      it "returns page_title - base_title" do
        page_title = "about"
        expect(full_title(page_title)).to eq("about - BIGBAG Store")
      end
    end

    context "there is not page_title" do
      it "returns base_title" do
        page_title = ""
        expect(full_title(page_title)).to eq("BIGBAG Store")
      end
    end
  end
end
